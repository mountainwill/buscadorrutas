#!/usr/bin/python3
import numpy as np, random, operator, pandas as pd, matplotlib.pyplot as plt
import sys
import json

# binary search implementation, returns the closest biggest item from the sorted list
def binarySearch(alist, item):
	first = 0
	last = len(alist)-1
	found = False

	while first<=last and not found:
		midpoint = (first + last)//2
		if alist[midpoint] == item:
			return alist[midpoint]
		else:
			if item < alist[midpoint]:
				last = midpoint-1
			else:
				first = midpoint+1
	try:
		return alist[first]
	except IndexError:
		return 1440

# node implementation, a gene can be a node; every station in the train system is a node
class Node:
	# the basic parameters are the nodes name and links (v)
	# an edge from v contains the next node and the cost
	def __init__(self, name, v = []):
		self.name = name
		self.v = v
	# setter for the links
	def setV(self, v):
		self.v = v
	# add a edge to the links
	def addArc(self, arc):
		self.v = self.v + [arc]
	# implementation of the cost function, calculates the cost to the next nod given the present time
	def cost(self, nod, time):
		next_bus_name = None
		next_bus = sys.maxsize
		the_cost = 0
		for edge in self.v:
			if edge[0] == nod:
				keys = list(schedule[str(self.name)].keys())
				# random.shuffle(keys)
				# print(schedule[str(self.name)])
				# random.shuffle(schedule[str(self.name)])
				for bus in keys:
					if(schedule[str(self.name)][bus]['next'] == nod.name):
						found = binarySearch(schedule[str(self.name)][bus]['times'], time)
						if(found < next_bus):
							next_bus = found
							# if(next_bus_)
							next_bus_name = bus
				the_cost = edge[1] + next_bus - time
				time = time + the_cost
				return [the_cost, time, next_bus, next_bus_name]
		return [None, None, None, None]
	# representation function, called when a node is printed
	def __repr__(self):
		return "Station: " + str(self.name)

# Fitness class, calculate the cost and fitness of a route, here, a route is also a individ
class Fitness:
	# initialisation, each object need a route and a initial time, the cost and bus_count are calculated internally
	def __init__(self, route, time):
		self.route = route
		self.cost = 0
		self.fitness= 0.0
		self.time = time
		# self.bus_count = 0
	# calculate the cost for the whole route
	def routeCost(self, mode):
		if self.cost == 0:
			pathCost = 0
			lastBusName = 'nada'
			for i in range(0, len(self.route) - 1):
				fromNod = self.route[i]
				toNod = None
				if i + 1 < len(self.route):
					toNod = self.route[i + 1]
				else:
					toNod = self.route[0]
				[temp_cost, self.time, nextBus, nextBusName] = fromNod.cost(toNod, self.time)
				if (lastBusName != nextBusName):
					# self.bus_count += 1
					# print(self.bus_count)
					pathCost += mode
				lastBusName = nextBusName
				if(temp_cost is None):
					self.cost = sys.maxsize
					return self.cost
				pathCost += temp_cost
			self.cost = pathCost
		return self.cost
	# calculate the route fitness, if mode is 0 than the fitness function ignores the number of bus changes
	# if mode is grater than 0 the bus changes are weighted accordingly 
	def routeFitness(self, mode):
		if self.fitness == 0:
			if(self.routeCost(mode) > 1500):
				self.fitness = 1/sys.maxsize
			else:
				self.fitness = 1 / (float(self.routeCost(mode))+1/sys.maxsize)
				# print(self.bus_count)
		return self.fitness
# function to create a valid random route between 2 nodes
# if the route gets larger than 3 times the number of stations, it stops there
def createRoute(nodList, firstNod, lastNod):
	route = [firstNod]
	size = len(nodList)
	i = 0
	while route[i] != lastNod and i < 3*size+1:
		nextElement = random.choice(route[i].v)
		route.append(nextElement[0])
		i = i + 1
	if route[i] != lastNod:
		route.append(lastNod)
	return route
# generate initial population
def initialPopulation(popSize, nodList):
	population = []
	for i in range(0, popSize):
		population.append(createRoute(nodList, firstNod, lastNod))
	return population
# rank a population
def rankRoutes(population, mode):
	fitnessResults = {}
	for i in range(0, len(population)):
		fitnessResults[i] = Fitness(population[i], initTime).routeFitness(mode)
	return sorted(fitnessResults.items(), key = operator.itemgetter(1), reverse = True)
# select the best individs from a population
def selection(popRanked, eliteSize):
	selectionResults = []
	df = pd.DataFrame(np.array(popRanked), columns=["Index","Fitness"])
	df['cum_sum'] = df.Fitness.cumsum()
	df['cum_perc'] = 100*df.cum_sum/df.Fitness.sum()
	for i in range(0, eliteSize):
		selectionResults.append(popRanked[i][0])
	for i in range(0, len(popRanked) - eliteSize):
		pick = 100*random.random()
		for i in range(0, len(popRanked)):
			if pick <= df.iat[i,3]:
				selectionResults.append(popRanked[i][0])
				break
	return selectionResults
# creates the mating pool from the best individs
def matingPool(population, selectionResults):
	matingpool = []
	for i in range(0, len(selectionResults)):
		index = selectionResults[i]
		matingpool.append(population[index])
	return matingpool
# breads two parents, if they don't have at least 1 gene in comon, copies one of them
def breed(parent1, parent2):
	child = []
	cross = list(set(parent1).intersection(parent2))
	if cross != []:
		crossAt = random.choice(cross)
		i = 0
		while(parent1[i] != crossAt):
			child.append(parent1[i])
			i = i + 1
		i = 0
		while(parent2[i] != crossAt):
			i = i + 1
		child = remove_loops(child + parent2[i:])
	else:
		child = random.choice([parent1, parent2])
	return child
# remove any loops in a list
def remove_loops(alist):
	loops = {}
	position = 0
	last_nod = -1
	for nod in alist:

		if nod not in loops:
			loops[nod] = [position, last_nod]
		else:
			if loops[nod][0] < position:
				loops[nod] = position
			else:
				position = loops[nod][0]
		last_nod = nod
	new_list = [last_nod]
	while loops[last_nod][1] != -1:
		new_list.append(loops[last_nod][1])
		last_nod = loops[last_nod][1]
	return new_list[::-1]
# select breed population from mating pool
def breedPopulation(matingpool, eliteSize):
	children = []
	length = len(matingpool) - eliteSize
	pool = random.sample(matingpool, len(matingpool))

	for i in range(0,eliteSize):
		children.append(matingpool[i])
	
	for i in range(0, length):
		child = breed(pool[i], pool[len(matingpool)-i-1])
		children.append(child)
	return children
# change a section of a cromosome, replace everything betwheen start and end with newGenom
def changeGenom(genom, start, end, newGenom):
	individual = []
	for i in range (0, len(genom)):
		if i < start:
			individual.append(genom[i])
		elif i > end:
			individual.append(genom[i])
		else:
			individual = individual + newGenom
			i = end
	return individual
# mutate a cromosome with a rate of mutationRate
# a mutation will randomly change a path between 2 random genes
def mutate(individual, mutationRate):
	for swapped in range(1, len(individual)-1):
		if(random.random() < mutationRate):
			swapWith = int(random.random() * len(individual))
			if swapped > swapWith:
				swapSegment = createRoute(stationDict, individual[swapped], individual[swapWith])
				changeGenom(individual, swapped, swapWith, swapSegment)
			elif swapped < swapWith:
				swapSegment = createRoute(stationDict, individual[swapWith], individual[swapped])
				changeGenom(individual, swapped, swapWith, swapSegment)
	return individual
# mutate the population
def mutatePopulation(population, mutationRate):
	mutatedPop = []
	
	for ind in range(0, len(population)):
		mutatedInd = mutate(population[ind], mutationRate)
		mutatedPop.append(mutatedInd)
	return mutatedPop
# calculate the next generation of individs
def nextGeneration(currentGen, eliteSize, mutationRate, mode):
	popRanked = rankRoutes(currentGen, mode)
	selectionResults = selection(popRanked, eliteSize)
	matingpool = matingPool(currentGen, selectionResults)
	children = breedPopulation(matingpool, eliteSize)
	nextGeneration = mutatePopulation(children, mutationRate)
	return nextGeneration
# implementation of genetic algorithm
def geneticAlgorithm(population, popSize, eliteSize, mutationRate, generations, mode):
	pop = initialPopulation(popSize, population)
	# print("Initial distance: " + str(1 / rankRoutes(pop, mode)[0][1]))
	
	for i in range(0, generations):
		pop = nextGeneration(pop, eliteSize, mutationRate, mode)
	
	# print("Final distance: " + str(1 / rankRoutes(pop, mode)[0][1]))
	bestRouteIndex = rankRoutes(pop, mode)[0][0]
	results = []
	bestRoute = pop[bestRouteIndex]
	temp = initTime
	for program in schedule:
		for i in range(0, len(buses)):
			for train in schedule[program]:
				if train == buses[i]:
					for time in range (0, len(schedule[program][train]['times'])):
						schedule[program][train]['times'][time] += i/100
	for i in range(0, len(bestRoute)-1):
		[temp, time, bus] = bestRoute[i].cost(bestRoute[i+1], temp)[1:4]
		time = round(time)
		results.append({'time': time, 'bus': bus, 'station': bestRoute[i].name})
	results.append({'station': bestRoute[len(bestRoute)-1].name})
	return results 
# implementation of genetic algorithm and plot the results
def geneticAlgorithmPlot(population, popSize, eliteSize, mutationRate, generations, mode):
	pop = initialPopulation(popSize, population)
	progress = []
	progress.append(1 / rankRoutes(pop, mode)[0][1])
	
	for i in range(0, generations):
		pop = nextGeneration(pop, eliteSize, mutationRate, mode)
		progress.append(1 / rankRoutes(pop, mode)[0][1])
	
	plt.plot(progress)
	plt.ylabel('Distance')
	plt.xlabel('Generation')
	plt.show()
# import station, links and schedule data from files
def importData(stations_file, arcs_file, schedule_file, buss_lines_file, pass_list):
	global buses
	stations = json.load(stations_file)
	arcs = json.load(arcs_file)
	schedule = json.load(schedule_file)
	stationDict = {}
	buses = json.load(buss_lines_file)
	# print(buses[0])

	for station in stations:
		idul = station['id']
		if idul not in pass_list:
			stationDict[idul] = Node(idul)
	for arc in arcs:
		origin = arc['origin']
		destination = arc['destination']
		duration = arc['duration']
		if origin not in pass_list and destination not in pass_list:
			stationDict[origin].addArc([stationDict[destination], duration])
			stationDict[destination].addArc([stationDict[origin], duration])
	return stationDict, schedule
# main function
def main():
	global firstNod
	global lastNod
	global initTime
	global schedule
	global stationDict
	global buses
	inputs = json.loads(sys.argv[1])
	stations_file = open('./extras/stations.json')
	arcs_file = open('./extras/arcs.json')
	schedule_file = open('./extras/schedulesMaped.json')
	buss_lines_file = open('./extras/busLineNames.json')
	pass_list = inputs['pass_list']
	stationDict, schedule = importData(stations_file, arcs_file, schedule_file, buss_lines_file, pass_list)
	firstNod = stationDict[inputs['first_station']]
	initTime = inputs['initial_time']
	VIA = inputs['via']
	mode = inputs['mode']
	if VIA > 0:
		midNod = stationDict[VIA]
		lastNod = midNod
		first_result = geneticAlgorithm(population = stationDict, popSize = 100, eliteSize = 20, 
			mutationRate = 0.1, generations = 20, mode=mode)
		del first_result[-1]
		firstNod = midNod
		lastNod = stationDict[inputs['last_station']]

		second_result = geneticAlgorithm(population = stationDict, popSize = 100, eliteSize = 20, 
			mutationRate = 0.1, generations = 20, mode=mode)
		result = first_result + second_result
	else:
		lastNod = stationDict[inputs['last_station']]
		result = geneticAlgorithm(population = stationDict, popSize = 100, eliteSize = 20, 
			mutationRate = 0.1, generations = 20, mode=mode)
	print(json.dumps(result))
	return 1

if __name__ == "__main__":
	main()
	# print(stationDict[238].v, file=sys.stderr)
	# print(stationDict[239].v, file=sys.stderr)
	# print(stationDict[279].v, file=sys.stderr)