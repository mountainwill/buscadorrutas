var $ = require('jquery');

var socket = io();


socket.on('error', console.error.bind(console));
socket.on('message', console.log.bind(console));


loadSvg = new Promise((resolve, reject)=>{
    $(document).ready(()=>{
        var a = document.getElementById("map");
        a.addEventListener("load",function(){
            var svgDoc = a.contentDocument; //get the inner DOM of alpha.svg
            var svgRoot  = svgDoc.documentElement;
            resolve(svgRoot)
        })
    })
})

// svg = $("#map").contentDocument.documentElement

var myData = {'first_station':"", 'last_station' :"", 'pass_list':[], 
                'initial_time':530, 'via': 0, 'mode': 1};
var waitMapInput = false;
var mySvg;
var stationNames = {};
var rgbColors=["#ff0000", "#333333", "#ff6600", "#ffff00", "#0000ff", "#999999", "#803300", "#ff00ff", "#00ff00", "#800080"];
var colors = ["ROJA", "NEGRA", "NARANJA", "AMARILLA",  "AZUL", "GRIS", "MARRON", "ROSA", "VERDE", "VIOLETA"]
var lineColors={};
for(i in colors){
    lineColors[colors[i]+"_I"] = i;
    lineColors[colors[i]+"_V"] = i;
}

var stationSelect = (station)=>{
    if(waitMapInput){
        $("."+waitMapInput, mySvg).removeClass(waitMapInput);
        $("."+waitMapInput).removeClass("selecting");
        $("."+waitMapInput).html(`${station.data("label")}: ${stationNames[station.data("label")]}`);
        myData[waitMapInput] = station.data("label");
        station.addClass(waitMapInput);
    }
    if(waitMapInput == "first_station"){
        waitMapInput = "last_station";
        $('.mapSelect.last_station').addClass("selecting");
    }
}

var drawPath = (drum)=>{
    $(".selectedSegment", mySvg).removeClass("selectedSegment");
    for(var d = 0; d < drum.length; d++){
        for(var i = 0; i < drum[d].stops.length; i++){
            next_stop = drum[d].stops[i+1] || ((drum[d+1] || {"stops":[]}).stops[0]);
            //console.log(`${lineColors[drum[d].bus]}_${drum[d].stops[i]}_${next_stop}`); 
            $(`[data-label=${lineColors[drum[d].bus]}_${drum[d].stops[i]}_${next_stop}]`, mySvg).addClass("selectedSegment");
            $(`[data-label=${lineColors[drum[d].bus]}_${next_stop}_${drum[d].stops[i]}]`, mySvg).addClass("selectedSegment");
        }
    }
}

var timeFormat = function(time){
    hours = ("00"+Math.floor(time/60)).slice(-2)
    minutes = ("00" + time%60).slice(-2)
    return `${hours} : ${minutes}`;
}

var viewPath= (drum)=>{
    $(".traseu").empty();

    var duration = drum.slice(-1)[0].arrivalTime - drum[0].time;
    $(".traseu").append($(`
        <div>Fastest route: ${duration} minutes:</div>
    `))
    for(d of drum){
        
        var startSt = d.stops[0];
        var endSt = d.stops[d.stops.length-1];
        var segm = $(`
            <div class = "busSegment">
                <div class = "busCol" style="background-color: ${rgbColors[lineColors[d.bus]]}"></div>
                <div>${startSt} : ${stationNames[startSt]}</div> <div>${timeFormat(d.time)}</div>
                <div>${endSt} : ${stationNames[endSt]}</div> <div >${timeFormat(d.arrivalTime)}</div>
                <div>${d.bus}</div> <div> Stops: ${JSON.stringify(d.stops)}</div> <div>Duration: ${d.duration}</div>
            </div>`)
        $(".traseu").append(segm);
    }
}

$(document).ready(()=>{
    loadSvg.then((svg)=>{
        mySvg = svg;
        var stationGroup = $("[data-label='Stations']", svg);
        stationGroup.addClass("stationGroup");
        stationGroup.click((e)=>{
            stationSelect($(e.target));
        })
    })
   
    $(".mapSelect").click((e)=>{
        waitMapInput = $(e.target).data("label");
        $(".mapSelect.selecting").removeClass("selecting");
        $(e.target).addClass("selecting");
        console.log("waiting for input: ", waitMapInput)
    })

    $(".searchModeInput").change((e)=>{
        myData.mode = $(e.target).val();
        console.log(myData);
    })

    $("#findButton").click((e)=>{
        myData.initial_time = 60*parseInt($("#departureHour").val()) + parseInt($('#departureMinute').val());
        console.log(myData);
        socket.emit('findRoute', myData);
    })

    socket.on('result', (drum)=>{
        console.log(drum);
        drawPath(drum);
        viewPath(drum);
        // $("#messages").text(JSON.stringify(drum));
    })
})

$.getJSON("stations.json", (stations)=>{
    stations.map(station=>{
        stationNames[station.id] = station.name;
    });
})