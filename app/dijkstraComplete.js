const fs = require('fs');
const tinyqueue = require('tinyqueue');

var stations = JSON.parse(fs.readFileSync("extras/stations.json"));
var vecini = JSON.parse(fs.readFileSync("extras/schedulesMaped.json"));

async function binarySearch(arr, val, cmp)
{
    cmp = cmp || ((a, b) => {return a - b;});
    var left = 0, right = arr.length - 1, middle;
    while(left <= right){
        middle = (left+right)>>>1;
        var comp = cmp(arr[middle], val);
        if(comp == 0)
            return {"found":true, "index": middle, "val":arr[middle]}
        else if(comp  < 0)
            left = middle + 1;
        else
            right = middle - 1;
    }
    return {"found": false, "index": left, "val": arr[left]}
}

async function dijkstra(origin, destination, startTime){
    best = {}, prec = {};
    for (st of stations) best[st] = -1;
    best[origin] = startTime;
    prec[origin] = {"station":"start"};

    var q = new tinyqueue([origin], (a, b)=>{return best[a.v] - best[b.v]})

    while(q.length > 0){
        try{
            nod = q.pop();
            if(nod == destination)
                break;
            for (var bus in vecini[nod]){
                var vec = vecini[nod][bus];
                nextTime = await binarySearch(vec.times, best[nod])
                var potAr = nextTime.val + parseInt(vec.duration)
                if(best[vec.next] == undefined ||  potAr < best[vec.next])
                {
                    best[vec.next] = potAr;
                    if(prec[vec.next] == undefined)
                        q.push(vec.next);
                    prec[vec.next] = {"station": nod, "bus": bus, "time":nextTime.val, "duration":vec.duration};
                }
            }

        }
        catch(err){
            console.log("problem with nod: " + nod);
            console.log(vecini[nod])
            console.log(err);
        }
    }

    drum = [{"station": destination}];
    while (prec[drum[drum.length-1].station] != undefined)
    {
        drum.push(prec[drum[drum.length-1].station]);
    }
    drum.reverse();

    var drum2 = drum.reduce(function(rv, x) {
        if(rv.length != 0 && rv[rv.length-1].bus == x.bus){
            rv[rv.length-1].stops += 1;
            rv[rv.length-1].duration += x.duration;
            rv[rv.length-1].arrivalTime += x.duration;
        }
        else{
            rv.push({...x, "stops": 1, "arrivalTime": x.time+x.duration})
        }
        return rv;
    }, [])

    return {"duration": best[destination] - startTime, "arrivalTime": best[destination], "traseu": drum, "detailed":drum2};
}

dijkstra(238, 279, 515).then(console.log);


