const fs = require('fs');
const tinyqueue = require('tinyqueue');

var stations = JSON.parse(fs.readFileSync("extras/stations.json"));
var arcs = JSON.parse(fs.readFileSync("extras/arcs.json"));

var vecini = {};

arcs.map(arc=>{
    (vecini[arc.origin] = vecini[arc.origin] || []).push({v:arc.destination, c:arc.duration});
    (vecini[arc.destination] = vecini[arc.origin] || []).push({v:arc.origin, c:arc.duration});
})

function dijkstra(origin, destination){
    best = {}, prec = {};
    for (st of stations) best[st] = -1;
    best[origin] = 0;
    prec[origin] = -1;

    var q = new tinyqueue([origin], (a, b)=>{return best[a] - best[b]})

    while(q.length > 0 && best[destination] == undefined){
        nod = q.pop();
        for (vec of vecini[nod]){
            if(best[vec.v] == undefined || best[nod] + vec.c < best[vec.v])
            {
                best[vec.v] = best[nod] + vec.c;
                if(prec[vec.v] == undefined)
                    q.push(vec.v);
                prec[vec.v] = nod;
            }
        }
    }

    drum = [destination];
    while (prec[drum[drum.length-1]] != undefined)
    {
        drum.push(prec[drum[drum.length-1]]);
    }
    drum.reverse();

    return {drum: drum, cost: best[destination]};
}

console.log(dijkstra(104, 238));


