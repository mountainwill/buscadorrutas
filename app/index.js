var http = require('http')
var fs = require('fs')
var express = require('express');
var app = express();
var path = require('path')
const child_process = require('child_process');

var stations = JSON.parse(fs.readFileSync("extras/stations.json"));
var vecini = JSON.parse(fs.readFileSync("extras/schedulesMaped.json"));
var arcs = JSON.parse(fs.readFileSync("extras/arcCosts.json"));


var server = app.listen(3040, function () {
    console.log('Example app listening on port 3040!');
  });
    
  
var io = require('socket.io').listen(server);

app.get('/', function (req, res) {
    res.sendFile(path.resolve('public/index.html'));
});

app.use(express.static(__dirname + '/public'));  

io.on('connection', function(socket) {
    socket.on('findRoute', (data)=>{
        console.log(JSON.stringify(data));
        proc = child_process.spawn("python", [path.resolve("genetic2v2.py"), JSON.stringify(data)]);
        socket.emit("data", "finding route...");
        proc.stdout.on('data', (data)=>{
            var drum = JSON.parse(data.toString('utf8'))
            console.log(drum);
            var drum2 = drum.reduce(function(rv, x) {
                if(x.bus){
                    if(rv.length == 0 || rv[rv.length-1].bus != x.bus)
                        rv.push({...x, "stops": [x.station], "arrivalTime": x.time, "duration": 0})
                    duration = vecini[x.station][x.bus].duration;
                    next = vecini[x.station][x.bus].next
                    rv[rv.length-1].stops.push(next);
                    rv[rv.length-1].duration += duration;
                    rv[rv.length-1].arrivalTime += duration;
                }
                return rv;
            }, [])
            console.log(drum2);
            socket.emit('result', drum2)
        })
        proc.stderr.on('data', (data)=>{
            console.log(data.toString());
        })
    })
});




