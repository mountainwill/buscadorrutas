var XLSX = require('xlsx')
const fs = require('fs');
var workbook = XLSX.readFile('rawData.xlsx');
var pprint = require('pprint')
var sheet_name_list = workbook.SheetNames;

// console.log(sheet_name_list)
var horario_start = XLSX.utils.sheet_to_json(workbook.Sheets['horarios']);
busLines = XLSX.utils.sheet_to_json(workbook.Sheets['busLines']);
var arcs = XLSX.utils.sheet_to_json(workbook.Sheets['arcs']);
var stations = XLSX.utils.sheet_to_json(workbook.Sheets['stations']);

var arcCosts = {};
stations.map(station=>{
    arcCosts[station.id] = {};
    arcs.filter(arc=>{return arc.origin == station.id}).map(arc=>{
        arcCosts[station.id][arc.destination] = arc.duration;
    })
    arcs.filter(arc=>{return arc.destination == station.id}).map(arc=>{
        arcCosts[station.id][arc.origin] = arc.duration;
    })
})

// console.log(arcCosts["104"])


busLinesNames = Object.keys(busLines[0])
busLinesRoutes = busLines.reduce((p, c) => {
    for(i in c){
        p[i] = p[i] ? p[i].concat(c[i]) : [c[i]];
    }
    return p;
  }, {});

//  console.log(busLinesRoutes);

horario_start = horario_start.map((x)=>{
    hora = x.Hora;
    delete x.Hora;
    for(key in x){
        
        x[key] = String(x[key]).split(",").map((y)=>{return parseInt(y)+60*parseInt(hora)})
    }
    return x;
}).reduce((p, c) => {
    for(i in c){
        p[i] = p[i] ? p[i].concat(c[i]) : c[i];
    }
    return p;
  }, {});

// console.log(horario_start);

busses = busLinesNames.map((name)=>{
    return {
        name: name,
        route: busLinesRoutes[name],
        startTimes: horario_start[name]
    }
})

function saveBusData()
{
    fs.writeFile("busData.json", JSON.stringify(busses), 'utf8', (err)=>{
        if(err)
            console.log(err);
        console.log("saved in file: busData.json");
    })
}

var stationSchedule = {};
stations.map((st)=>{
    stationSchedule[st.id] = []
})

function busRouteSim(busName, stationId, routeLeft, times)
{
    try
    {
        if(routeLeft.length == 0) return;
        for(time of times)
            stationSchedule[stationId].push({"next": routeLeft[0], "bus": busName, "time": time});
        var nexTimes = times.map(time=>{return time+arcCosts[stationId][routeLeft[0]] + 1});
        busRouteSim(busName, routeLeft[0], routeLeft.slice(1, routeLeft.end), nexTimes)
    }
    catch(err)
    {
        console.log("problem with bus: " + busName + " station: " +stationId);
        console.log(routeLeft);
    }
}

busses.map( bus =>{
     busRouteSim(bus.name, bus.route[0], bus.route.slice(1, bus.route.end), bus.startTimes);
})

stationBusSchedule = {};
for (k in stationSchedule){
    stationSchedule[k].sort((a, b)=>{return a.time - b.time})
    stationBusSchedule[k] = stationSchedule[k].reduce(function(rv, x) {
        (rv[x["bus"]] = rv[x["bus"]] || {"next": x.next, "duration":arcCosts[k][x.next], "times": []}).times.push(x.time);
        return rv;
    }, {})
};

//  console.log(stationBusSchedule['64']["ROJA_V"]);
 // fs.writeFileSync("extras/arcCosts.json", JSON.stringify(arcCosts));


console.log(busLinesNames);
fs.writeFileSync("extras/busLineNames.json", JSON.stringify(busLinesNames));




